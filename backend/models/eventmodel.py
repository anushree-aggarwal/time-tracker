from db import db


class TimeEvent(db.Model):
	__tablename__ = 'timeevent'
	id = db.Column(db.Integer, primary_key=True)
	folder = db.Column(db.Text, nullable=False)
	created_on = db.Column(db.DateTime, server_default=db.func.now())

	def __init__(self, folder):

		self.folder = folder

	def save_to_db(self):

		db.session.add(self)
		db.session.commit()

	def delete(self):

		db.session.delete(self)
		db.session.commit()

	def json(self):
		return {'folder':self.folder,'time':str(self.created_on)}

	@staticmethod
	def get_everything():
		return TimeEvent.query.all()
	